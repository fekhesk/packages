# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="rust"
VERSION="1.63.0"
EPOCH=0
DESC="Memory-safe programming language without garbage collection."
GRPS=()
URL="https://www.rust-lang.org"
LICENSES=("Apache-2.0" "MIT")
DEPENDS=("glibc" "gcc-libs" "llvm" "libffi")
OPT_DEPENDS=()
MK_DEPENDS=("python" "cmake" "ninja" "curl" "openssl" "git")
PROVIDES=("rust" "cargo")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://static.rust-lang.org/dist/rustc-${VERSION}-src.tar.gz")

SUM_TYPE="sha512"
SUM=("8c4ce20f0e6ddf86c13e24f222846dbcddc6b8fccc5490b1c41877df3ecf2543bf5fd88431d2e6d23280f90c0395f0d812b4ad7c13098128b043669660911299")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}c-${VERSION}-src"

cat << EOF > config.toml
[llvm]
link-shared = true

[build]
locked-deps = true
vendor = true
sanitizers = true
extended = true

[install]
prefix = "/usr"
docdir = "share/doc/rust-${VERSION}"

[rust]
channel = "stable"
rpath = false
description = "Rust v${VERSION}-${EPOCH} - Packaged for yiffOS on $(date)"

# FileCheck isn't installed, disable codegen tests
codegen-tests = false

[target.x86_64-unknown-linux-gnu]
llvm-config = "/usr/bin/llvm-config"

[target.i686-unknown-linux-gnu]
llvm-config = "/usr/bin/llvm-config"
EOF

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}c-${VERSION}-src"

    RUSTFLAGS="$RUSTFLAGS -C link-args=-lffi" \
    python3 ./x.py build

    # Some tests do fail, about 50ish tests
    python3 ./x.py test --verbose --no-fail-fast

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}c-${VERSION}-src"

    DESTDIR="${BUILD_DATA_ROOT}" python3 ./x.py install

    # Install stdlib
    DESTDIR="${BUILD_DATA_ROOT}" python3 ./x.py install --target=x86_64-unknown-linux-gnu std
    DESTDIR="${BUILD_DATA_ROOT}" python3 ./x.py install --target=i686-unknown-linux-gnu std

    return 0
}