# Package Maintainers
MAINTAINERS=("Evie Viau <evie@eviee.gay>")

# Package information
NAME="flatpak"
VERSION="1.14.0"
EPOCH=1
DESC="Linux application sandboxing and distribution framework."
GRPS=()
URL="https://flatpak.org/"
LICENSES=("LGPL-2.1")
DEPENDS=("appstream-glib" "bubblewrap" "dconf" "fuse" "gdk-pixbuf2" "glib2" "glibc" "json-glib" "libxau" "libarchive" "curl" "gcc-libs" "librsvg2" "libseccomp" "libxml2" "zstd" "libostree" "polkit" "systemd")
OPT_DEPENDS=()
PROVIDES=("flatpak")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://github.com/flatpak/flatpak/releases/download/${VERSION}/flatpak-${VERSION}.tar.xz"
    "https://flathub.org/repo/flathub.flatpakrepo")

SUM_TYPE="sha512"
SUM=("88f011534a8da6c2421d7d24043756bfe6c46957b6d09ba96b269002a5f0d0372b51f4a8643af2020ef4ed8c5b5f102994ddab54a7052cfba15f8e3b160e8a83"
    "752cdb2f0f4774ac6966033edc989456764cf8fe9a2a97918c75ecfd47b2dacb567e98ede00168f780a8fc97ae6e413bb550ebd2323cb4aefb578469d9338a4d")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    ./configure                                             \
        --prefix=/usr                                       \
        --sysconfdir=/etc                                   \
        --localstatedir=/var                                \
        --sbindir=/usr/bin                                  \
        --libexecdir=/usr/lib                               \
        --disable-static                                    \
        --disable-documentation                             \
        --with-system-bubblewrap                            \
        --with-system-dbus-proxy                            \
        --with-dbus-config-dir=/usr/share/dbus-1/system.d

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    DESTDIR="${BUILD_DATA_ROOT}" make install

    # Add flatpak profile scripts from repo
    install -Dt "${BUILD_DATA_ROOT}/etc/profile.d" -m644 ${WORKDIR}/${NAME}-${VERSION}/profile/flatpak.sh
    install -Dt "${BUILD_DATA_ROOT}/etc/profile.d" -m644 ${WORKDIR}/${NAME}-${VERSION}/profile/flatpak.fish

    install -Dt "${BUILD_DATA_ROOT}/etc/flatpak/remotes.d/" ${WORKDIR}/flathub.flatpakrepo

    install -d -o root -g 102 -m 750 "${BUILD_DATA_ROOT}/usr/share/polkit-1/rules.d"

    return 0
}